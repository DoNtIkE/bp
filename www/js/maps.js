var MAP = null;
var LAYERS = [];

// Nastavení vrstev. Lze nastavit specificky pro kazdy typ nebo jen pro skupinu testů.
var LAYERS_SETTINGS = [];
LAYERS_SETTINGS["TouristicRoutesTest-red"] = {"color": "red", "weight": 4, "opacity": 1.0};
LAYERS_SETTINGS["TouristicRoutesTest-green"] = {"color": "green", "weight": 4, "opacity": 1.0};
LAYERS_SETTINGS["TouristicRoutesTest-blue"] = {"color": "blue", "weight": 4, "opacity": 1.0};
LAYERS_SETTINGS["TouristicRoutesTest-yellow"] = {"color": "yellow", "weight": 4, "opacity": 1.0};
LAYERS_SETTINGS["TouristicRoutesTest-bicycle"] = {"color": "#ff00ff", "weight": 4, "opacity": 1.0};

LAYERS_SETTINGS["MissingTagTest"] = {"color": "#FFA726", "weight": 10, "opacity": 0.3};
LAYERS_SETTINGS["WrongValueTagTest"] = {"color": "#FFA726", "weight": 10, "opacity": 0.3};
LAYERS_SETTINGS["ConflictTagTest"] = {"color": "#EF5350", "weight": 10, "opacity": 0.3};

LAYERS_SETTINGS["GuidepostsTest-ok"] = {icon:  L.icon({iconUrl: "./images/gp_ok.png", iconSize: [48, 48], iconAnchor: [23, 45]})};
LAYERS_SETTINGS["GuidepostsTest-without-photo"] = {icon:  L.icon({iconUrl: "./images/gp_without-photo.png", iconSize: [48, 48],iconAnchor: [23, 45]})};
LAYERS_SETTINGS["GuidepostsTest-without-ref"] = {icon:  L.icon({iconUrl: "./images/gp_without-ref.png", iconSize: [48, 48], iconAnchor: [23, 45]})};
LAYERS_SETTINGS["GuidepostsTest-without-ref-photo"] = {icon:  L.icon({iconUrl: "./images/gp_without-ref-photo.png",iconSize: [48, 48],iconAnchor: [23, 45]})};

function setAnalyzeLayer(){
	var test = $('input[name=visibleLayers]:checked').data("test");
	var type = $('input[name=visibleLayers]:checked').data("type");
	getLayerData(test, type);
}

function setTouristicRoutes(){
	if($('input[name=touristicRoutes]').is(':checked')){
		getLayerData("TouristicRoutesTest", "red");
		getLayerData("TouristicRoutesTest", "green");
		getLayerData("TouristicRoutesTest", "blue");
		getLayerData("TouristicRoutesTest", "yellow");
	}
}


function getLayerData(test, type){
	$.ajax({
		type: 'POST',
		url: './ajax/maps-data/',
		data: {
			ajax: '1',
			test: test,
			type: type,
			bounds: MAP.getBounds().toBBoxString(),
			zoom: MAP.getZoom()
		}
	}).done(function( json ) {
		json = $.parseJSON(json);
		if(json) setLayerData(json);
	});
}

function renderRelations(data){
	var dataLayer = L.geoJson(data, {
		onEachFeature: function(feature, layer) {
			// chtělo by to načíst ajaxem snippet a sem to jen replacnout...
			var popupText = "Destinations: " + feature.properties.destinations
				+ "<br>Type: " + feature.properties.type
				+ "<br>Route: " + feature.properties.route
				+ "<br>Network: " + feature.properties.network
				+ "<br>Complete: " + feature.properties.complete
				+ "<br>Operator: " + feature.properties.operator;
			layer.bindPopup(popupText); 
		},
		style: function () {
			if( data.testName+"-"+data.testType in LAYERS_SETTINGS ){
				return LAYERS_SETTINGS[data.testName+"-"+data.testType];
			}
			else if( data.testName in LAYERS_SETTINGS ){
				return LAYERS_SETTINGS[data.testName];
			}
		}
	});
	return dataLayer;
}

function renderNodes(data){
	var geojsonMarkerOptions = {
		radius: 8,
		fillColor: "#ff7800",
		color: "#000",
		weight: 1,
		opacity: 1,
		fillOpacity: 0.8
	};

	var dataLayer = L.geoJson(data, {
		onEachFeature: function(feature, layer) {
			// chtělo by to načíst ajaxem snippet a sem to jen replacnout...
			var popupText = "name: " + feature.properties.name
				+ "<br>ref: " + feature.properties.ref;
			layer.bindPopup(popupText); 
		},
		pointToLayer: function (feature, latlng) {
				var style = null;
				if( data.testName+"-"+data.testType in LAYERS_SETTINGS ){
					style = LAYERS_SETTINGS[data.testName+"-"+data.testType];
				}
				else if( data.testName in LAYERS_SETTINGS ){
					style = LAYERS_SETTINGS[data.testName];
				}
			return L.marker(latlng, style);
			}
		});
	return dataLayer;
}

function setLayerData(data){
	removeLayer(data.testName, data.testType);

	var layer = null;
	if(data.entryType == "relations"){
		layer = renderRelations(data);
	}
	else if(data.entryType == "nodes"){
		layer = renderNodes(data);
	}

	layer.addTo(MAP);
	LAYERS[data.testName+"-"+data.testType] = layer;
}

function removeLayer(test, type){
	if( test+"-"+type in LAYERS ){
		MAP.removeLayer(LAYERS[test+"-"+type]);
	}
}


$( document ).ready(function() {

	// Příprava rozhraní pro mapu
	$("#footer").hide();
	$("body").css("margin-bottom", "0px");
	$("#mymap").height($(window).height()-$('nav').height());


	$('input[name=visibleLayers]').on('change', function(){
		if($(this).is(':checked')){
			setAnalyzeLayer();
		}
		else{
			var test = $(this).data("test");
			var type = $(this).data("type");
			removeLayer(test, type);
		}
		
	});

	// Vykreslení turistických tras
	$('input[name=touristicRoutes]').on('change', function(){		
		if($(this).is(':checked')){
			setTouristicRoutes();
		}
		else{
			removeLayer("TouristicRoutesTest", "red");
			removeLayer("TouristicRoutesTest", "green");
			removeLayer("TouristicRoutesTest", "blue");
			removeLayer("TouristicRoutesTest", "yellow");
		}
	});
});





jQuery(window).load(function() {	
		MAP = L.map('mymap');
		MAP.setView([49.1922467,16.6240757], 14);
		L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			maxZoom: 18,
			attribution: 'Map data &copy; OpenStreetMap contributors'
		}).addTo(MAP);

		// Rozšíření Leaflet
		var sidebar = L.control.sidebar('sidebar', {position: 'right'}).addTo(MAP);
		L.control.scale().addTo(MAP);

		MAP.on('moveend zoomend', function() { 
			setAnalyzeLayer();
			setTouristicRoutes();
		});
});