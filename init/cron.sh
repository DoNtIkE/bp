#!/bin/bash

# path cron file
CRON_FILE="/etc/crontab"
DOMAIN="http://bp.local/"

echo "1 * * * *" wget $DOMAIN"longtest/hourly" >> $CRON_FILE
echo "1 0 * * *" wget $DOMAIN"longtest/daily"  >> $CRON_FILE
echo "1 0 1 * *" wget $DOMAIN"longtest/monthly"  >> $CRON_FILE
