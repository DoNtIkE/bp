--
-- Name: test_executions; Type: TABLE; Schema: hicheck; Owner: postgres
--

-- Create by: pg_dump osmhicheck --user postgres --password --format=plain --schema=hicheck --schema-only > dump.sql

CREATE TABLE test_executions (
    id integer NOT NULL,
    test_id integer NOT NULL,
    date timestamp without time zone NOT NULL
);


ALTER TABLE test_executions OWNER TO postgres;

--
-- Name: test_executions_entries; Type: TABLE; Schema: hicheck; Owner: postgres
--

CREATE TABLE test_executions_entries (
    id integer NOT NULL,
    execution_id integer NOT NULL,
    entry_type character varying(200) NOT NULL,
    state character varying,
    geom geometry,
    entry_id bigint NOT NULL,
    data text
);


ALTER TABLE test_executions_entries OWNER TO postgres;

--
-- Name: test_executions_entries_id_seq; Type: SEQUENCE; Schema: hicheck; Owner: postgres
--

CREATE SEQUENCE test_executions_entries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_executions_entries_id_seq OWNER TO postgres;

--
-- Name: test_executions_entries_id_seq; Type: SEQUENCE OWNED BY; Schema: hicheck; Owner: postgres
--

ALTER SEQUENCE test_executions_entries_id_seq OWNED BY test_executions_entries.id;


--
-- Name: test_executions_id_seq; Type: SEQUENCE; Schema: hicheck; Owner: postgres
--

CREATE SEQUENCE test_executions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_executions_id_seq OWNER TO postgres;

--
-- Name: test_executions_id_seq; Type: SEQUENCE OWNED BY; Schema: hicheck; Owner: postgres
--

ALTER SEQUENCE test_executions_id_seq OWNED BY test_executions.id;


--
-- Name: test_stats; Type: TABLE; Schema: hicheck; Owner: postgres
--

CREATE TABLE test_stats (
    id integer NOT NULL,
    test_id integer NOT NULL,
    date date NOT NULL,
    count integer NOT NULL,
    success integer NOT NULL
);


ALTER TABLE test_stats OWNER TO postgres;

--
-- Name: test_stats_id_seq; Type: SEQUENCE; Schema: hicheck; Owner: postgres
--

CREATE SEQUENCE test_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE test_stats_id_seq OWNER TO postgres;

--
-- Name: test_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: hicheck; Owner: postgres
--

ALTER SEQUENCE test_stats_id_seq OWNED BY test_stats.id;


--
-- Name: tests; Type: TABLE; Schema: hicheck; Owner: postgres
--

CREATE TABLE tests (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    type character varying(200) NOT NULL
);


ALTER TABLE tests OWNER TO postgres;

--
-- Name: tests_id_seq; Type: SEQUENCE; Schema: hicheck; Owner: postgres
--

CREATE SEQUENCE tests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tests_id_seq OWNER TO postgres;

--
-- Name: tests_id_seq; Type: SEQUENCE OWNED BY; Schema: hicheck; Owner: postgres
--

ALTER SEQUENCE tests_id_seq OWNED BY tests.id;

