<?php
class AjaxController extends Controller{
	protected $view = "";

	public function control(array $url){

		if($url[2] == "getDataGraph"){
			
			if(isset($_POST['graphName']) && isset($_POST['interval'])){
				$stats = new Stats($this->db);
				$data = $stats->getDataGraph($_POST['graphName'], $_POST['interval']);
				echo json_encode($data);
				die;
			}
		}

		if($url[2] == "maps-data"){

			if(isset($_POST['test']) && isset($_POST['type'])){
				$testName = $_POST['test'];
				$type = $_POST['type'];
				$bounds = explode(",", $_POST['bounds']);
				
				$test = new $testName($this->db);
				$entries = $test->prepareTest(array("type" => $type, "bounds" => $bounds))->execTest(array("bounds" => $bounds))->getResult();

				$ret = array();
				$ret["testName"] = $testName;
				$ret["testType"] = $type;
				$ret["type"] = "FeatureCollection";
				$ret["features"] = array();

				if($testName != "GuidepostsTest"){
					$ret["entryType"] = "relations";

					if( ($testName == "TouristicRoutesTest") && ($type == "bicycle")){
						$relationsBound = $this->getRelationsInBoundCyclo($bounds);
					}
					else{
						$relationsBound = $this->getRelationsInBound($bounds);
					}

					if(is_array($entries) && count($entries)){
						$entries = array_intersect_key($entries, $relationsBound);

						foreach ($entries as $entry) {
							$sql = "SELECT member_id, ST_AsGeoJSON(linestring) as way FROM relation_members INNER JOIN ways ON relation_members.member_id = ways.id WHERE relation_members.relation_id = ".$entry["id"]."  ORDER BY sequence_id";
							$result = $this->db->query($sql);

							if($result){
								while($row = pg_fetch_assoc($result)){
									$coordinates = array();
									$feature = array();

									$sql = "SELECT hstore_to_json(tags) AS tags FROM relations WHERE id = '".$entry["id"]."'";
									$feature["properties"] = json_decode($this->db->selectOne($sql));
									$feature["type"] = "Feature";
									$feature["geometry"]["type"] = "LineString";
									
									$data = json_decode($row["way"]);

									foreach ($data->coordinates as $value) {
										$coordinates[] = $value;
									}

									$zoom = Out::getFilterFromZoom($_POST["zoom"]);
									$feature["geometry"]["coordinates"] = Out::efficientFilter($coordinates, 0.000005);
									$ret["features"][] = $feature;
								}
							}
						}
					}
				}
				else{
					$ret["entryType"] = "nodes";

					$nodesBound = $this->getNodesInBound($bounds);

					if(is_array($entries) && count($entries)){
						$entries = array_intersect_key($entries, $nodesBound);

						foreach ($entries as $entry) {								
							$feature = array();
							$sql = "SELECT hstore_to_json(tags) AS tags, ST_AsText(geom) AS geom FROM nodes WHERE id = '".$entry["id"]."'";

							$row = $this->db->selectOneRow($sql);


							$feature["properties"] = json_decode($row['tags']);
							$feature["type"] = "Feature";
							$feature["geometry"]["type"] = "Point";
							
							$coordinates = array_reverse(explode(" ", Out::getLatFromPoint($row["geom"])));

							$feature["geometry"]["coordinates"] = $coordinates;
							$ret["features"][] = $feature;
						}
					}
				}

				echo json_encode($ret);
				die;
			}
		}
	}


	private function getRelationsInBound(array $bounds){
		$sql = "SELECT DISTINCT relation_id as route FROM relation_members INNER JOIN ways on relation_members.member_id = ways.id INNER JOIN relations on relation_members.relation_id = relations.id WHERE ways.linestring && ST_MakeEnvelope(".$bounds[0].", ".$bounds[1].", ".$bounds[2].", ".$bounds[3].") AND ".Osm::NOT_CYCLO;
		$result = $this->db->query($sql);
		$ret = array();
		if($result){
			while($row = pg_fetch_assoc($result)){
				$ret[$row["route"]] = "";
			}
		}
		return $ret;
	}

	private function getRelationsInBoundCyclo(array $bounds){
		$sql = "SELECT DISTINCT relation_id as route FROM relation_members INNER JOIN ways on relation_members.member_id = ways.id INNER JOIN relations on relation_members.relation_id = relations.id WHERE ways.linestring && ST_MakeEnvelope(".$bounds[0].", ".$bounds[1].", ".$bounds[2].", ".$bounds[3].")";
		$result = $this->db->query($sql);
		$ret = array();
		if($result){
			while($row = pg_fetch_assoc($result)){
				$ret[$row["route"]] = "";
			}
		}
		return $ret;
	}

	private function getNodesInBound(array $bounds){
		$sql = "SELECT id FROM nodes WHERE geom && ST_MakeEnvelope(".$bounds[0].", ".$bounds[1].", ".$bounds[2].", ".$bounds[3].") AND tags @> '\"information\"=>\"guidepost\"'::hstore";
		$result = $this->db->query($sql);
		$ret = array();
		if($result){
			while($row = pg_fetch_assoc($result)){
				$ret[$row["id"]] = "";
			}
		}
		return $ret;
	}
}