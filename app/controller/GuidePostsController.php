<?php

class GuidePostsController extends Controller{
	protected $view = "guidePosts";

	public function control(array $url){

		$this->header = array(
			"title" => "GuidePosts",	
			"keywords" => "OSM, ",	
			"description" => "Automatické testování tagů KČT",
		);

		if($url[2] == "fetch"){
			$guidePosts = new GuidepostsTest($this->db);
			$guidePosts->prepareTest(array());

			$this->data['distance'] = $guidePosts->getDistance();
			$this->data["count"] = $guidePosts->getInsertedEntries();
			$this->view = "guidePostsFetch";
		}
		else if($url[2] == "analyse"){
			$time = time();
			$guidePosts = new GuidepostsTest($this->db);
			$guidePosts->execTest(array("analyse"));

			$unused = $guidePosts->getUnusedGp();

			// Save result into file
			$guidePosts->getGpxContent();
			$guidePosts->getJsonContent();

			$this->data['nodes'] = $guidePosts->getResult();
			$this->data['execTime'] = time()-$time;
			$this->data['distance'] = $guidePosts->getDistance();
			$this->data['countAll'] = count($guidePosts->getResult());
			$this->data['countOk'] = count($guidePosts->getResultOk());
			$this->data['countWithoutRef'] = count($guidePosts->getResultWithoutRef());
			$this->data['countWithoutPhoto'] = count($guidePosts->getResultWithoutPhoto());
			$this->data['countWithoutRefAndPhoto'] = count($guidePosts->getResultWithoutRefAndPhoto());
			$this->data['countUnused'] = count($unused);
			$this->data['unusedGuideposts'] = $unused;
			$this->view = "guidePostsAnalyse";
		}

		else if( ($url[2] == "cached") || ($url[2] == "")){

			$this->view = "guideposts_cached";
			$guidePosts = new GuidepostsTest($this->db);
			$guidePosts->execTest(array());
			$this->data['distance'] = $guidePosts->getDistance();


			if($url[3] == "ok"){
				$this->data['description'] = "Guideposts with ref tag and associated image - no further action required.";
				$this->data['nodes'] = $guidePosts->getResultOk();

			}
			else if($url[3] == "without-ref-photo"){
				$this->data['description'] = "Guideposts with no ref tag/value and without existing image - need to get photo and then update node's tag in OSM.";
				$this->data['nodes'] = $guidePosts->getResultWithoutRefAndPhoto();
			}
			else if($url[3] == "without-photo"){
				$this->data['description'] = "";
				$this->data['nodes'] = $guidePosts->getResultWithoutPhoto();
				
			}
			else if($url[3] == "without-ref"){
				$this->data['description'] = "Guideposts with no ref tag/value but with existing image - need to update node's tag in OSM.";
				$this->data['nodes'] = $guidePosts->getResultWithoutRef();
			
			}
			else if($url[3] == "unused"){
				$this->data['description'] = "Unused images - no guidepost found closer than 20m. Either move the image or the guidepost or there is no node for guidepost in OSM at all. Skipped images with tags: infotabule, mapa, cyklo, necitelne, znaceni, zruseno, prehledova, zahranicni";
				$this->data['nodes'] = $guidePosts->getUnusedGp();
			
			}
			else{
				$this->data['description'] = "";
				$this->data['nodes'] = $guidePosts->getResult();
			}
			
			$this->data['countEntries'] = count($this->data['nodes']);
		}	
	}
}