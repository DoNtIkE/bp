<?php

class TablesController extends Controller{
	protected $view = "tables";

	public function control(array $url){

		$this->header = array(
			"title" => "Tabulky",	
			"keywords" => "OSM, ",	
			"description" => "Automatické testování tagů KČT",
		);
	
		$missing = MissingTagTest::$types;
		$wrong = WrongValueTagTest::$types;
		$conflict = ConflictTagTest::$types;

		if(isset($url[3])){
			// Výběr akce
			if( ($url[2] == "missing") && in_array($url[3], $missing)){
				$missingTag = new MissingTagTest($this->db);
				$this->data['relations'] = $missingTag->prepareTest(array("type" => $url[3]))->execTest(array())->getResult();
				$this->data['title'] = "Počet KČT turistických relací v databázi";
				$this->view = "missing_".$url[3];
			}
			else if( ($url[2] == "wrong") && in_array($url[3], $wrong)){
				$wrongTag = new WrongValueTagTest($this->db);
				$this->data['relations'] = $wrongTag->prepareTest(array("type" => $url[3]))->execTest(array())->getResult();
				$this->data['title'] = "Nalezené rozpory tagů";
				$this->view = "wrong_".$url[3];
			}
			else if( ($url[2] == "conflict") && in_array($url[3], $conflict)){
				$conflictTag = new ConflictTagTest($this->db);
				$this->data['relations'] = $conflictTag->prepareTest(array("type" => $url[3]))->execTest(array())->getResult();
				$this->data['title'] = "Nalezené rozpory tagů";
				$this->view = "conflict_".$url[3];
			}
			
		}
		else {
			// Výpis všech testů
			$statsMissing = new StatsMissingRelationsTest($this->db);
			$this->data['missing'] = $statsMissing->prepareTest(array())->execTest(array())->getResult();

			$statsWrong = new StatsWrongRelationsTest($this->db);
			$this->data['wrong'] = $statsWrong->prepareTest(array())->execTest(array())->getResult();

			$statsConflict = new StatsConflictRelationsTest($this->db);
			$this->data['conflict'] = $statsConflict->prepareTest(array())->execTest(array())->getResult();

			// Výsledek Long testu
			$guideposts = new StatsGuidepostsTest($this->db);
			$this->data['guideposts'] = $guideposts->prepareTest(array())->execTest(array())->getResult();

			$this->view = "tables";
		}
	}
}