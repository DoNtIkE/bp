<?php

abstract class Controller{

	protected $db;
	protected $conf;
	protected $data = array();
	protected $view = "";
	protected $header = array('title' => '', 'keywords' => '', 'description' => '');

	public function __construct(Connector $db, Configuration $conf){
		$this->db = $db;
		$this->conf = $conf;
	}

	abstract public function control(array $url);

	public function printView(){
		if ($this->view){
			extract($this->data);
			require(__DIR__."/../view/" . $this->view . ".phtml");
		}
	}
}