<?php

class StatsController extends Controller{
	protected $view = "stats";

	public function control(array $url){
		$this->header = array(
			"title" => "Statistik of test",	
			"keywords" => "Osm, automatic test",	
			"description" => "Longtest execution",
		);

		$stats = new Stats($this->db);
		$this->data['graphs'] = $stats->getGraphsNames();


	}
}