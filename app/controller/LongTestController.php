<?php

class LongTestController extends Controller{
	protected $view = "longtest";

	public function control(array $url){
		$this->header = array(
			"title" => "Longtest",	
			"keywords" => "Osm, automatic test",	
			"description" => "Longtest execution",
		);

		if($url[2] == "hourly"){


			$this->data['title'] = "Long test hourly";
		}
		else if($url[2] == "daily"){			
			$guidePosts = new GuidepostsTest($this->db);
			$guidePosts->prepareTest(array("type" => "ok"));
			$guidePosts->execTest(array('analyse'));

			$count = count($guidePosts->getResult());
			$ok = count($guidePosts->getResultOk());
			$percentage = Out::getPercentage($ok, $count);
			$this->data['result']["GuidePosts"] = array("count" => $count, "ok" => $ok, "percentage" => $percentage);


			// Zavolám všechny testy pro vytvoření statistik
			$statsConflict = new StatsConflictRelationsTest($this->db);
			$statsConflict->prepareTest(array())->execTest(array());

			$statsMissing = new StatsMissingRelationsTest($this->db);
			$statsMissing->prepareTest(array())->execTest(array());

			$statsWrong = new StatsWrongRelationsTest($this->db);
			$statsWrong->prepareTest(array())->execTest(array());


			$this->data['title'] = "Long test daily";
		}
		else if($url[2] == "monthly"){

			$this->data['title'] = "Long test monthly";
		}
	}
}