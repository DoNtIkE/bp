<?php

class RouterController extends Controller{

	protected $view = "layout";
	protected $controller;

	public function control(array $url){
		$controller = "";
		$url[1] = strtolower($url[1]);

		if( ($url[1] == "maps") || ($url[1] == "")){
			$controller = "MapsController";
		}
		else if($url[1] == "tables"){
			$controller = "TablesController";
		}
		else if($url[1] == "guideposts"){
			$controller = "GuidePostsController";
		}
		else if($url[1] == "ajax"){
			$controller = "AjaxController";
			$this->view = "layoutClear";
		}
		else if($url[1] == "longtest"){
			$controller = "LongTestController";
		}
		else if($url[1] == "stats"){
			$controller = "StatsController";
		}
		else {
			if(file_exists(BASE."404.html")){
				http_response_code(404);
				echo file_get_contents(BASE."404.html");
			}
			die;
		}

		$this->controller = new $controller($this->db, $this->conf);
		$this->controller->control($url);

		$this->data['title'] = $this->controller->header['title'];
		$this->data['keywords'] = $this->controller->header['keywords'];
		$this->data['description'] = $this->controller->header['description'];
	}

	public function parseUrl($url){
		return explode("/", $url);
	}
}