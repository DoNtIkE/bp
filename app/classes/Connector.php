<?php
abstract class Connector{

	private static $instance = null;
	private $connection = null;


	abstract public function query($sql);
	abstract public function selectOne($sql);
	abstract public function selectOneRow($sql);

	abstract public function __construct($server, $user, $password, $database);

	abstract public static function getInstance($server, $user, $password, $database);

	// Duplication object not allowed
	private function __clone() {}
}