<?php

class ErrorOrWarningException extends Exception{
	protected $context = null;
	protected $file, $line;

	public function __construct($code, $message, $file, $line, $context){
		parent::__construct($message, $code);

		$this->file = $file;
		$this->line = $line;
		$this->context = $context;
	}

	public function getContext(){
		return $this->context;
	}
}