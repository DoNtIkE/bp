<?php

class TouristicRoutesTest extends Test{

	protected $sql;
	protected $result = array();

	public static $types = array("all", "red", "blue", "green", "yellow", "bicycle");

	public function prepareTest(array $params){

		$where = " WHERE ".Osm::NOT_CYCLO;
		if(isset($params['type'])){
			if ($params["type"] == "all"){
				$where .= "";
			}
			else if($params['type'] == "red"){
				$where .= " AND exist(relations.tags,'kct_red')";
				$this->sql = "SELECT id FROM relations ".$where;
			}
			else if($params['type'] == "blue"){
				$where .= " AND exist(relations.tags,'kct_blue')";
				$this->sql = "SELECT id FROM relations ".$where;
			}
			else if($params['type'] == "green"){
				$where .= " AND exist(relations.tags,'kct_green')";
				$this->sql = "SELECT id FROM relations ".$where;
			}
			else if($params['type'] == "yellow"){
				$where .= " AND exist(relations.tags,'kct_yellow')";
				$this->sql = "SELECT id FROM relations ".$where;
			}
			else if($params['type'] == "bicycle"){
				//if(isset($params["bounds"])){
					$this->sql = "SELECT DISTINCT relation_id as id FROM relation_members INNER JOIN ways on relation_members.member_id = ways.id INNER JOIN relations on relation_members.relation_id = relations.id WHERE ways.linestring && ST_MakeEnvelope(".$params["bounds"][0].", ".$params["bounds"][1].", ".$params["bounds"][2].", ".$params["bounds"][3].") AND relations.tags->'route' ='bicycle';";
				//}
			}
			else{
				throw new TestNotFoundException;
			}

		}
		return $this;
	}

	public function execTest(array $params){
		$pg_result = $this->db->query($this->sql);
		if($pg_result){
			while($row = pg_fetch_assoc($pg_result)){
				$this->result[$row["id"]] = array("id" => $row["id"]);
			}		
		}
		//echo count($this->result);die;
		return $this;
	}
}