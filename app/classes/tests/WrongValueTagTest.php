<?php

class WrongValueTagTest extends Test{

	protected $sql;
	protected $result;
	protected $type;

	public static $types = array("all", "network", "complete", "osmc", "route", "kct");

	public function prepareTest(array $params){
		$where = Osm::NOT_CYCLO;

		if(isset($params["type"])){
			$this->type =  $params["type"];
		}
		
		$this->sql = "SELECT id, hstore_to_json(tags) AS tags FROM relations WHERE ".$where;
		return $this;
	}

	public function execTest(array $params){
		$pg_result = $this->db->query($this->sql);
		if($pg_result){			
			while($row = pg_fetch_assoc($pg_result)){
				$row['tags'] = json_decode($row['tags'], true);

				if($this->type == "kct"){					
					if(!$this->checkKctTagValue(Osm::getKctTags($row['tags']))){
						$row["class"] = "wrong";
						$this->result[$row["id"]] = $row;
					}
				}
				else if($this->type == "network"){
					if(isset($row['tags']['network']) && !$this->checkNetworkTagValue($row['tags']['network'])){
						$row["class"] = "wrong";
						$this->result[$row["id"]] = $row;
					}
				}
				else if($this->type == "complete"){
					if(isset($row['tags']['complete']) && !$this->checkCompleteTagValue($row['tags']['complete'])){
						$row["class"] = "wrong";
						$this->result[$row["id"]] = $row;
					}
				}
				else if($this->type == "osmc"){
					if(isset($row['tags']['osmc:symbol']) && !$this->checkOsmcTagValue($row['tags']['osmc:symbol'])){
						$row["class"] = "wrong";
						$this->result[$row["id"]] = $row;
					}
				}else if($this->type == "route"){
					if(isset($row['tags']['route']) && !$this->checkRouteTagValue($row['tags']['route'])){
						$row["class"] = "wrong";
						$this->result[$row["id"]] = $row;
					}
				}
				else if($this->type == "all"){
					//TODO
					;
				}
				else{
					throw new TestNotFoundException;
				}

			}
		}

		return $this;
	}


	protected function checkKctTagValue($kct_tags){
		$correctKeys = Osm::getKctKeyList();
		$correctValues  = Osm::getKctValueList();
		foreach ($kct_tags as $key => $value) {
			// Pokud není klíč i hodnota v předdefinovaných hodnotách
			if(!in_array($key, $correctKeys) || !in_array($value, $correctValues)){
				return false;
			}
		}
		return true;
	}

	protected function checkNetworkTagValue($tag){
		$correctValues = Osm::getNetworkValueList();
		if(in_array($tag, $correctValues[0]) || in_array($tag, $correctValues[1])) return true;
		return false;
	}

	protected function checkCompleteTagValue($tag){
		$correctValues = Osm::getCompleteValueList();
		if(in_array($tag, $correctValues)) return true;
		return false;
	}

	protected function checkRouteTagValue($tag){
		$correctValues = Osm::getRouteValueList();
		if(in_array($tag, $correctValues)) return true;
		return false;
	}

	protected function checkOsmcTagValue($tag){
		$osmc = Osm::getOsmcValues($tag);
		$correctValues = Osm::getOsmcValueList();

		if(in_array($osmc[0], $correctValues['f_color']) &&
	       in_array($osmc[1], $correctValues['b_color']) &&
	       in_array($osmc[2], $correctValues['symbol'])){
	        return true;
	    }
	    return false;
	}
}