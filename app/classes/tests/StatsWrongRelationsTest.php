<?php

class StatsWrongRelationsTest extends Test{
	public function __construct(Connector $db){
		$this->db = $db;

		// uložím si test do DB pokud tam ještě nejsou
		$this->saveTests("WrongValueTagTest", WrongValueTagTest::$types);
		$this->typesWithId = $this->getTypesOfTest("WrongValueTagTest");
	}

	public function prepareTest(array $params){
		return $this;
	}

	public function execTest(array $params){
		$missingTagTest = new MissingTagTest($this->db);
		$countRelations = count($missingTagTest->prepareTest(array("type" => "all"))->execTest(array())->getResult());
		
		foreach (WrongValueTagTest::$types as $param) {
			$test = new WrongValueTagTest($this->db);
			$count = count($test->prepareTest(array("type" => $param))->execTest(array())->getResult());

			$this->result[$param] = array("count" => $count);
			$this->result[$param]["percentage"] = Out::getPercentage($count, $countRelations);

			// Uložím si statistiky
			if($param == "all"){
				$this->saveStats($this->typesWithId[$param], $countRelations, $countRelations);
			}
			else{
				$this->saveStats($this->typesWithId[$param], $countRelations, $count);
			}
		}

		return $this;
	}
}