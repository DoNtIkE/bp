<?php

class GuidepostsTest extends Test{
	const DISTANCE = 20;
	const GPX_FILE = EXPORT_BASE."guideposts.gpx";
	const JSON_FILE = EXPORT_BASE."guideposts.json";
	const IMG_FILE = EXPORT_BASE."unused_img.txt";

	public static $types = array("all", "ok", "without-photo", "without-ref", "without-ref-photo");

	protected $typesWithId = array();

	public function __construct(Connector $db){
		$this->db = $db;

		// uložím si test do DB pokud tam ještě nejsou
		$this->saveTests(get_class($this), self::$types);
		$this->typesWithId = $this->getTypesOfTest(get_class($this));

		// init reuslts array
		foreach (self::$types as $value) {
			$this->result[$value] = array();
		}
	}

	public function prepareTest(array $params){
		$this->type = $params["type"];
		return $this;
	}

	public function execTest(array $params){
		// Pokud se má znovu analyzovat
		if(in_array("analyse", $params)){
			$this->analyse();
		}
		else{
			$last_execution = $this->getLastExecutionOfTest($this->getTestId(get_class($this), self::$types[0]));

			// zkusím načíst z DB pokud se již dnes analyzovalo
			if(date("y-m-d") == date("y-m-d", strtotime($last_execution["date"]))){
				$this->loadFromCache();
			}
			else{
				$this->analyse();
			}
		}

		return $this;
	}


	protected function analyse(){

		$content = file_get_contents('http://api.openstreetmap.cz/table/all?output=geojson');
		if(strlen($content) < 10000){
			throw new Exception("Invalid length response");
		}

		// uvolnění DB
		$this->truncateTable('hicheck.guideposts');

		$json = json_decode($content);
		foreach($json->features as $future){

			$sql = "INSERT INTO hicheck.guideposts(id, by, url, ref, geom) VALUES('".$future->properties->id."', '".$future->properties->attribution."', '".$future->properties->url."', '".$future->properties->ref."',ST_GeomFromText('POINT(".$future->geometry->coordinates[0]." ".$future->geometry->coordinates[1].")', 4326));";
			$this->db->query($sql);
		}

		foreach (self::$types as $type) {
			$this->saveExecution($this->typesWithId[$type]);
		}

		$img_skip = array();		
		$img_skip = $this->filterUnused('http://api.openstreetmap.cz/table/hashtag/infotabule?output=json');
		$img_skip = array_merge($img_skip, $this->filterUnused('http://api.openstreetmap.cz/table/hashtag/mapa?output=json'));
		$img_skip = array_merge($img_skip, $this->filterUnused('http://api.openstreetmap.cz/table/hashtag/cyklo?output=json'));
		$img_skip = array_merge($img_skip, $this->filterUnused('http://api.openstreetmap.cz/table/hashtag/necitelne?output=json'));
		$img_skip = array_merge($img_skip, $this->filterUnused('http://api.openstreetmap.cz/table/hashtag/znaceni?output=json'));
		$img_skip = array_merge($img_skip, $this->filterUnused('http://api.openstreetmap.cz/table/hashtag/zruseno?output=json'));
		
		$sql="SELECT id, ST_AsText(geom) AS geom, tags->'ref' AS ref, tags->'name' AS name FROM nodes WHERE tags @> '\"information\"=>\"guidepost\"'::hstore AND id NOT IN('".implode("', '", array_values($img_skip))."') ORDER BY id";
		$result = $this->db->query($sql);
		$i=0;
		while($node = pg_fetch_assoc($result)){
			$sql = "SELECT id, ref, by, ST_AsText(geom) AS geom, ST_DistanceSphere(n, g.geom) AS dist FROM hicheck.guideposts AS g, ST_GeomFromText('".$node['geom']."') AS n WHERE ST_DistanceSphere(n, g.geom) <".self::DISTANCE;
			$result_gp = $this->db->query($sql);

			$gp = array();
			if($result_gp && pg_num_rows($result_gp)){
				while($row = pg_fetch_assoc($result_gp)){
					$gp[] = $row;
				}
			}
			$node['data'] = $gp;

			// Used guideposts
			$img = array();
			foreach ($node['data'] as $value) {
				$img[] = $value['id'];
			}

			// check guideposts attributes
			if(isset($node['ref'])){
				if(count($node['data'])){
					$node['class'] = "ok";
					$this->result["ok"][$node['id']] = $node;

					$type = "ok";
				}
				else{
					$node['class'] = "without-photo";
					$this->result["without-photo"][$node['id']] = $node;

					$type = "without-photo";
				}
			}
			else{
				if(count($node['data'])){
					$node['class'] = "without-ref";
					$this->result["without-ref"][$node['id']] = $node;

					$type = "without-ref";
				}
				else{
					$node['class'] = "without-ref-photo";
					$this->result["without-ref-photo"][$node['id']] = $node;

					$type = "without-ref-photo";
				}
			}

			$last_execution = $this->getLastExecutionOfTest($this->typesWithId[$type]);
			$this->saveEntry($last_execution['id'], $node['id'], 'nodes', '', json_encode($img));

			// Všechny uzly
			$this->result["all"][] = $node;

			
			$last_execution = $this->getLastExecutionOfTest($this->typesWithId['all']);
			$this->saveEntry($last_execution['id'], $node['id'], 'nodes', $type, json_encode($img));
			

			// Pro debugování na localu si to zkrátím
			$i++;
			//if(LOCAL && ($i > 100)) break;
		}
		// uložím statistiku
		$count_all = count($this->getResult());
		foreach (self::$types as $value) {
			$success = count($this->result[$value]);
			$this->saveStats($this->typesWithId[$value], $count_all, $success);
		}
		return $this;
	}

	public function loadFromCache(){
		foreach(self::$types as $type){
			$testId = $this->typesWithId[$type];
			$lastExecution = $this->getLastExecutionOfTest($testId);
			$entries = $this->getEntries($lastExecution['id']);

			foreach ($entries as $entry) {
				$node = $this->getNode($entry['entry_id']);

				$guideposts_id = json_decode($entry['data']);
				$node['data'] = array();
				foreach($guideposts_id as $id){
					$gp = $this->getGuidepostById($id);
					$gp['dist'] = $this->getDistancePoints($gp['geom'], $node['geom']);

					$node['data'][] = $gp;
				}

				if($type == "all"){
					$node["class"] = $entry['state'];
				}
				else{
					$node['class'] = $type;	
				}
				$this->result[$type][$node['id']] = $node;
			}
		}
		return $this;
	}

	protected function getNode($node_id){
		$sql = "SELECT id, ST_AsText(geom) AS geom, tags->'ref' AS ref, tags->'name' AS name FROM nodes WHERE id = '".$node_id."' LIMIT 1";
		return $this->db->selectOneRow($sql);
	}

	protected function getGuidepostById($guidepost_id){
		$sql = "SELECT id, by, url, ref, ST_AsText(geom) AS geom FROM guideposts WHERE id = '".$guidepost_id."' LIMIT 1";
		return $this->db->selectOneRow($sql);
	}

	protected function getDistancePoints($geom1, $geom2){
		$sql = "SELECT ST_DistanceSphere(geom1, geom2) AS dist FROM ST_GeomFromText('".$geom1."') AS geom1, ST_GeomFromText('".$geom2."') AS geom2";
		return $this->db->selectOne($sql);
	}

	protected function filterUnused($url){
		$ret = array();
		$content = json_decode(file_get_contents($url));
		foreach($content as $i){
			$ret[] = $i[0];
		}
		return $ret;
	}

	public function getUsedGp(){
		$used = array();
		foreach ($this->result["all"] as $node) {
			if(count($node['data'])){
				foreach ($node['data'] as $value) {
					$used[$value['id']] = $value;
				}
			}
		}
		return $used;
	}

	public function getUnusedGp(){
		$used = $this->getUsedGp();
		$sql = "SELECT id, ref, by, ST_AsText(geom) AS geom FROM hicheck.guideposts WHERE geom && ST_MakeEnvelope(12.09, 51.06, 18.87, 48.55, 4326) AND id NOT IN('".implode("', '", array_keys($used))."')";
		$result = $this->db->query($sql);
		$ret = array();
		if($result && pg_num_rows($result)){
			while($row = pg_fetch_assoc($result)){
				$ret[$row['id']] = $row;
			}
		}
		return $ret;
	}

	public function getJsonContent(){
		$nodes = $this->getResultWithoutRef();
		$nodes = array_merge($nodes, $this->getResultWithoutPhoto());
		$nodes = array_merge($nodes, $this->getResultWithoutRefAndPhoto());

		$geojson = array(
			'type'	=> 'FeatureCollection',
			'features'	=> array()
		);

		foreach($nodes as $node){
			preg_match('/POINT\(([-0-9.]{1,8})[0-9]* ([-0-9.]{1,8})[0-9]*\)/', $node['geom'], $matches);
			$lon = $matches[1];
			$lat = $matches[2];
			$name = isset($node->name) ? htmlspecialchars($node['name']) : 'n'.$node['id'];

			if(isset($node['ref'])){
				if(!count($node['data'])){
					$class = "noimg";
				}
			}
			else{
				if(count($node['data'])){
					$class = "noref";
				}
				else{
					$class = "missing";
				}
			}

			$geojson['features'][] = $this->getNodeGeoJson($node['id'], $lon, $lat, $name, $class);
		}
		file_put_contents(self::JSON_FILE, json_encode($geojson));
	}

	protected function getNodeGeoJson($id, $lon, $lat, $name, $class){
		$feature = array(
			'id' => $id,
			'type' => 'Feature', 
			'geometry' => array(
				'type' => 'Point',
				'coordinates' => array($lon, $lat)
			),
			'properties' => array(
				'name' => $name,
				'class' => $class,
				)
			);
		return $feature;
	}

	public function getGpxContent(){

		$nodes = $this->getResultWithoutRef();
		$nodes = array_merge($nodes, $this->getResultWithoutPhoto());
		$nodes = array_merge($nodes, $this->getResultWithoutRefAndPhoto());

		$gpx_header = '<?xml version="1.0" encoding="utf-8" standalone="yes" ?'.">\n";
		$gpx_header .= '<gpx version="1.1" creator="OsmHiCheck"'."\n";
		$gpx_header .= '  xmlns="http://www.topografix.com/GPX/1/1"'."\n";
		$gpx_header .= '  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'."\n";
		$gpx_header .= '  xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">'."\n";	

		$gpx_content = "";
		foreach($nodes as $node){
			$gpx_content .= $this->getNodeGpx($node);
		}
		file_put_contents(self::GPX_FILE, $gpx_header.$gpx_content."</gpx>");
	}

	public function getNodeGpx($node){
		//check if need to put to GPX and GeoJSON
		preg_match('/POINT\(([-0-9.]{1,8})[0-9]* ([-0-9.]{1,8})[0-9]*\)/', $node['geom'], $matches);
		$lon = $matches[1];
		$lat = $matches[2];
		$name = isset($node->name) ? htmlspecialchars($node['name']) : 'n'.$node['id'];

		$gpx_content = "<wpt lat=\"$lat\" lon=\"$lon\">"."\n";
		$gpx_content .= "<name>$name</name>"."\n";
		$gpx_content .= "<desc>n".$node['id']."</desc>"."\n";
		
		if(isset($node['ref'])){
			$gpx_content .= '<sym>misc-sunny</sym>'."\n";
		}
		else{
			if(count($node['data'])){
				$gpx_content .= '<sym>transport-accident</sym>'."\n";
			}
			else{
				$gpx_content .= '<sym>misc-sunny</sym>'."\n";
			}
		}
		$gpx_content .= '</wpt>'."\n";
		return $gpx_content;
	}

	public function getResult(){
		if(isset($this->result[$this->type])) return $this->result[$this->type];
		return array();
	}

	public function getResultOk(){
		return $this->result["ok"];
	}

	public function getResultByParam($param){
		if(isset($this->result[$param])) return $this->result[$param];
		return array();
	}

	public function getResultWithoutRef(){
		if(isset($this->result["without-ref"])) return $this->result["without-ref"];
		return array();
	}

	public function getResultWithoutPhoto(){
		return $this->result["without-photo"];
	}

	public function getResultWithoutRefAndPhoto(){
		return $this->result["without-ref-photo"];
	}

	public function getInsertedEntries(){
		$sql = "SELECT count(*) FROM hicheck.guideposts";
		return $this->db->selectOne($sql);
	}

	public function getDistance(){
		return self::DISTANCE;
	}

	protected function truncateTable($table){
		$result = $this->db->query('TRUNCATE TABLE '.$table.';');
	}
}