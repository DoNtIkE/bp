<?php

class MissingTagTest extends Test{

	protected $sql;
	protected $result = array();

	public static $types = array("all", "not_network", "not_complete", "not_osmc", "not_dest", "not_any_tags");

	public function prepareTest(array $params){

		$where = Osm::NOT_CYCLO;
		if(isset($params['type'])){
			if ($params["type"] == "all"){
				$where .= "";
			}
			else if($params['type'] == "not_network"){
				$where .= " AND not exist(relations.tags,'network')";
			}
			else if($params['type'] == "not_complete"){
				$where .= " AND not exist(relations.tags,'complete')";
			}
			else if($params['type'] == "not_osmc"){
				$where .= " AND not exist(relations.tags,'osmc:symbol')";
			}
			else if($params['type'] == "not_dest"){
				$where .= " AND not exist(relations.tags,'destinations')";
			}
			else if($params['type'] == "not_any_tags"){
				$where .= " AND (not exist(relations.tags,'network') OR not exist(relations.tags,'complete') OR not exist(relations.tags,'osmc:symbol') OR not exist(relations.tags,'destinations'))";
			}
			else{
				throw new TestNotFoundException;
			}
		}
		$this->sql = "SELECT id, hstore_to_json(tags) AS tags FROM relations WHERE ".$where;
		return $this;
	}

	public function execTest(array $params){
		$pg_result = $this->db->query($this->sql);
		if($pg_result){			
			
			while($row = pg_fetch_assoc($pg_result)){
				$this->result[$row["id"]] = $row;				
				$this->result[$row["id"]]['tags'] = json_decode($row['tags'], true);
			}
		}

		return $this;
	}
}