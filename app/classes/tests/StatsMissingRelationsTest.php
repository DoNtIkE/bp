<?php

class StatsMissingRelationsTest extends Test{
	public function __construct(Connector $db){
		$this->db = $db;

		// uložím si test do DB pokud tam ještě nejsou
		$this->saveTests("MissingTagTest", MissingTagTest::$types);
		$this->typesWithId = $this->getTypesOfTest("MissingTagTest");
	}

	public function prepareTest(array $params){
		return $this;
	}

	public function execTest(array $params){
		foreach (MissingTagTest::$types as $param) {
			$test = new MissingTagTest($this->db);
			$count = count($test->prepareTest(array("type" => $param))->execTest(array())->getResult());

			$this->result[$param] = array("count" => $count);
			$this->result[$param]["percentage"] = Out::getPercentage($count, $this->result['all']['count']);

			// Uložím si statistiky
			$this->saveStats($this->typesWithId[$param], $this->result['all']['count'], $count);
		}

		return $this;
	}
}