<?php

class StatsGuidepostsTest extends Test{
	public function __construct(Connector $db){
		$this->db = $db;

		$this->typesWithId = $this->getTypesOfTest("GuidepostsTest");
	}

	public function prepareTest(array $params){
		return $this;
	}

	public function execTest(array $params){
		$guideposts = new GuidepostsTest($this->db);
		$guideposts->execTest(array());

		foreach (GuidepostsTest::$types as $param) {
			
			$count = count($guideposts->getResultByParam($param));

			$this->result[$param] = array("count" => $count);
			$this->result[$param]["percentage"] = Out::getPercentage($count, $this->result["all"]["count"]);

		}
		return $this;
	}
}