<?php

abstract class Test{

	protected $db = null;
	protected $result = array();

	public static $types = array();

	public function __construct(Connector $db){
		$this->db = $db;

		$this->saveTests(get_class($this), self::$types);
	}

	public abstract function prepareTest(array $params);
	public abstract function execTest(array $params);

	public function getResultJSON(){
		return json_encode($this->result);
	}

	public function getResult(){
		return $this->result;
	}


	public static function getTypes(){
		return self::$types;
	}

	public function saveTests($name, $types){
		if(count($types)){
			foreach ($types as $value) {
				$sql = "INSERT INTO tests(name, type) VALUES('".$name."', '".$value."') ON CONFLICT (name, type) DO NOTHING;;";
				$this->db->query($sql);
			}
		}
		return $this;
	}

	public function getTypesOfTest($name){
		$sql = "SELECT id, type FROM tests WHERE name = '".$name."'";
		$result = $this->db->query($sql);

		$ret = array();
		if($result){
			while($row = pg_fetch_assoc($result)){
				$ret[$row['type']] = $row['id'];
			}
		}
		return $ret;
	}

	public function getTestId($name, $type){
		$sql = "SELECT id FROM tests WHERE name = '".$name."' AND type = '".$type."' LIMIT 1";
		return $this->db->selectOne($sql);
	}

	protected function saveExecution(int $test_id){
		$datetime = date("y-m-d H:i:s");
		$sql = "INSERT INTO test_executions(test_id, date) VALUES('".$test_id."', now()::timestamp);";
		return $this->db->query($sql);
	}

	protected function getLastExecutionOfTest($test_id){
		$sql = "SELECT * FROM test_executions WHERE test_id = '".$test_id."' ORDER BY date DESC LIMIT 1;";
		return $this->db->selectOneRow($sql);
	}

	protected function saveStats(int $test_id, $count, $success){
		$date = date("Y-m-d");
		$sql = "INSERT INTO test_stats(test_id, date, count, success) VALUES('".$test_id."', '".$date."', '".$count."', '".$success."') ON CONFLICT(test_id,date) DO UPDATE SET count = '".$count."', success = '".$success."';";
		return $this->db->query($sql);
	}


	protected function saveEntry(int $execution_id, $entry_id, $entry_type, $state, $data){
		$sql = "INSERT INTO test_executions_entries(execution_id, entry_id, entry_type, state, data) VALUES('".$execution_id."', '".$entry_id."', '".$entry_type."', '".$state."', '".$data."')";
		return $this->db->query($sql);
	}

	protected function getEntries(int $execution_id){
		$sql = "SELECT * FROM test_executions_entries WHERE execution_id = '".$execution_id."'";
		$result = $this->db->query($sql);
		return $this->db->fetchAllAssoc($result);
	}

}