<?php

class ConflictTagTest extends Test{

	protected $sql;
	protected $result;
	protected $type;

	public static $types = array("all", "network_kct", "route_type", "bicycle", "route_color");

	public function prepareTest(array $params){
		$where = Osm::NOT_CYCLO;

		if(isset($params["type"])){
			$this->type =  $params["type"];
		}
		
		$this->sql = "SELECT id, hstore_to_json(tags) AS tags FROM relations WHERE ".$where;
		return $this;
	}

	public function execTest(array $params){
		$pg_result = $this->db->query($this->sql);
		if($pg_result){			
			while($row = pg_fetch_assoc($pg_result)){
				$row['tags'] = json_decode($row['tags'], true);

				if($this->type == "network_kct"){
					$kct = Osm::getKctTags($row["tags"]);
					if(isset($row['tags']['network']) && count($kct) && !$this->checkNetworkKct($row["tags"]["network"], $kct)){
						$row["class"] = "conflict";
						$this->result[$row['id']] = $row;
					}
				}
				else if($this->type == "route_type"){
					$kct = Osm::getKctTags($row["tags"]);
					if(isset($row['tags']['osmc:symbol']) && isset($row['tags']['route']) && !$this->checkTagOsmcKctRoute($row['tags']['osmc:symbol'], $kct, $row['tags']['route'])){
						$row["class"] = "conflict";
						$this->result[$row['id']] = $row;
					}
				}
				else if($this->type == "bicycle"){
					$kct = Osm::getKctTags($row["tags"]);
					if(isset($row['tags']['ref']) && isset($row['tags']['network']) && (reset($kct) == "bicycle") && !$this->checkRefNetwork($row['tags']['network'], $row['tags']['ref'])){
						$row["class"] = "conflict";
						$this->result[$row['id']] = $row;
					}
				}
				else if($this->type == "route_color"){
					$kct = Osm::getKctTags($row["tags"]);
					if(isset($row['tags']['osmc:symbol']) && !$this->checkOsmcKctColor($row['tags']['osmc:symbol'], $kct)){
						$row["class"] = "conflict";
						$this->result[$row['id']] = $row;
					}
				}
				else if($this->type == "all"){
					//TODO 
					;
				}
				else{
					throw new TestNotFoundException;
				}

			}
		}

		return $this;
	}


	protected function checkNetworkKct($network, $kctTags){
		$kct = reset($kctTags);

		// Zkontroluji hodnotu Kct tagu
	    if(!$this->checkKctTagValue($kctTags)){
	        return false;
	    }
	    
	    // Zkontroluji hodnotu Network tagu
	    if(!$this->checkNetworkTagValue($network)){
	    	return false;
	    }
	    
	    if(trim($kct) != 'bicycle'){
	    	$networkList = Osm::getNetworkValueList();
	        if(!in_array($network, $networkList[0])){
	            return false;
	        }

	        $table = Osm::getNetworkKctTable();
	        return $table[$kct][$network] == 1 ? true : false;
	    }
	    return true;
	}

	protected function checkOsmcKctColor($osmcTag, $kctTags){
		$osmc = Osm::getOsmcValues($osmcTag);

		$kct = key($kctTags);
		if($this->checkOsmcTagValue($osmcTag) && $kct){

			$kctColor = str_replace("kct_", "", $kct);
	    	if($osmc[0] == $kctColor){
	    		return true;
	    	}
	    	else{
	    		return false;
	    	}
		}
		return true;
	}

	protected function checkRefNetwork($network, $ref){
		if(is_numeric($ref) && $ref > 0){
			if( ($network == "lcn") && ($ref >= 1000) ) return true;
			if( ($network == "rcn") && ($ref >= 1000) ) return true;
			if( ($network == "ncn") && ($ref < 1000) ) return true;
		}
		return false;
	}

	protected function checkTagOsmcKctRoute($osmcTag, $kctTag, $route){
	    $osmc = Osm::getOsmcValues($osmcTag);
	    $route_kct = Osm::getRouteKctTable();
	    $route_osmc = Osm::getRouteOsmcTable();
	    $kct_osmc = Osm::getKctOsmcTable();

	    // Zkontroluji hodnoty tagu zda jsou validní
    	$tmp = Osm::getKctTags($kctTag);
	    $kct = reset($tmp);
	    if($this->checkKctTagValue($kctTag) && $this->checkOsmcTagValue($osmcTag) && $this->checkRouteTagValue($route) && count($tmp)){
		    if (!in_array($kct, $route_kct[$route])) { // kct vs route
		        return false;
		    }
		    if (!in_array($osmc[1], $route_osmc[$route]) || $osmc[1] != $route_osmc[$route][0]) { //kontrola symbolu || kontrola barvy pozadi
		        return false;
		    }
		    if ($kct_osmc[$kct][0] != $osmc[2] || $kct_osmc[$kct][1] != $osmc[1]) {  //kontrola symbolu || kontrola barvy pozadi
		        return false;
		    }
	    }
	    return true;
	}


	protected function checkKctTagValue($kct_tags){
		$correctKeys = Osm::getKctKeyList();
		$correctValues  = Osm::getKctValueList();
		foreach ($kct_tags as $key => $value) {
			// Pokud není klíč i hodnota v předdefinovaných hodnotách
			if(!in_array($key, $correctKeys) || !in_array($value, $correctValues)){
				return false;
			}
		}
		return true;
	}

	protected function checkNetworkTagValue($tag){
		$correctValues = Osm::getNetworkValueList();
		if(in_array($tag, $correctValues[0]) || in_array($tag, $correctValues[1])) return true;
		return false;
	}

	protected function checkOsmcTagValue($tag){
		$osmc = Osm::getOsmcValues($tag);
		$correctValues = Osm::getOsmcValueList();

		if(in_array($osmc[0], $correctValues['f_color']) &&
	       in_array($osmc[1], $correctValues['b_color']) &&
	       in_array($osmc[2], $correctValues['symbol'])){
	        return true;
	    }
	    return false;
	}

	protected function checkRouteTagValue($tag){
		$correctValues = Osm::getRouteValueList();
		if(in_array($tag, $correctValues)) return true;
		return false;
	}
}