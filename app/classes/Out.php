<?php

class Out {

	/**
	* Return percentage from total
	* @param int $part 
	* @param int $total
	* @return float percentage
	*/
	public static function getPercentage($part, $total){
		if($part == 0) return 0;
		if($total == 0) return 0;
		else if($part == $total) return 100.00;
		else{
			return round((100 / $total) * $part, 2);
		}
	}

	public static function getLatFromPoint($geom){
		return preg_replace('/POINT\(([-0-9.]{1,8})[0-9]* ([-0-9.]{1,8})[0-9]*\)/', '$2 $1', $geom);
	}



	
	public static function printCell($value){
		$ret = '<td>';
		$ret .= $value;
		$ret .= '</td>';
		return $ret;
	}

	public static function printEmtpyCell(){
		$ret = '<td class="missing">';
		$ret .= '&nbsp;';
		$ret .= '</td>';
		return $ret;
	}

	public static function printWrongCell($value){
		$ret = '<td class="conflict">';
		$ret .= $value;
		$ret .= '</td>';
		return $ret;
	}


	/**
	 * slozitejsi filtr na zaklade vypoctu smeru vektoru
	 * @author: PETR ŠVAŇA, 2015
	 * @modified: PATRIK LEV, 2017
	 * @param $nodes
	 * @param $filter
	 * @return array
	 */
	public static function efficientFilter($nodes, $filter){
		$cnt = count($nodes);
		$filtered = array();
		$filtered[] = $nodes[0];
		$vector_prev = array();
		$vector_next = array();
		$vector_prev[0] = $nodes[1][0] - $nodes[0][0];
		$vector_prev[1] = $nodes[1][1] - $nodes[0][1];
		for ($i = 1; $i < $cnt-2; $i++){
			$vector_next[0] = $nodes[$i+1][0] - $nodes[$i][0];
			$vector_next[1] = $nodes[$i+1][1] - $nodes[$i][1];
			$x = abs($vector_next[0] - $vector_prev[0]);
			$y = abs($vector_next[1] - $vector_prev[1]);
			$vector_prev = $vector_next;
			if($x > $filter && $y > $filter){
				$filtered[] = $nodes[$i];
			}
		}
		$filtered[] = $nodes[$cnt-1];
		return $filtered;
	}

	/**
	 * funkce pro prispusobeni filtru na zaklade zoomu
 	 * @author: PETR ŠVAŇA, 2015
	 * @modified: PATRIK LEV, 2017
	 * @param $zoom
	 * @return float|int
	 */
	public static function getFilterFromZoom($zoom){
		if($zoom >= 16) return 0;
		else if($zoom <16 && $zoom >= 15) return 0.000005;	
		else if($zoom <15 && $zoom >= 13) return 0.00001;
		else if($zoom == 12) return 0.0008;
		else return 1;
	}


}