<?php
/**
 * @author: PETR ŠVAŇA, 2015
 * @modified: PATRIK LEV, 2017
 */

class Osm{

	const NOT_CYCLO = "relations.tags->'route'!='bicycle' and 
	(
    not exist(relations.tags, 'kct_green') or
    not exist(relations.tags, 'kct_blue') or
    not exist(relations.tags, 'kct_yellow') or
    not exist(relations.tags, 'kct_red') or
    not exist(relations.tags, 'kct_none')
    )";


	public static function getKctTags($tags){
		$ret = array();
		foreach ($tags as  $key => $value) {
			if(preg_match('|^kct_(.+?)$|', $key)){
				$ret[$key] = $value; 
			}
		}
		return $ret;
	}
	public static function getOsmcValues($osmc){
		$ret = NULL;
		$ret = explode(":", $osmc);
		if(count($ret) == 3){
			$tmp = explode("_", $ret[2]);
			$ret[2] = $tmp[count($tmp)-1];
		}
		return $ret;
	}

	public static function getKctKeyList(){
		$key = array("kct_red", "kct_green", "kct_blue", "kct_yellow", "kct_white", "kct_black", "kct_purple", "kct_none", "kct_orange", "kct_barva");
		return $key;
	}

	public static function getKctValueList(){
		$values = array("local", "major", "ski", "learning", "horse", "wheelchair", "peak", "spring", "ruin", "bicycle", "interesting_object",);
		return $values;
	}

	public static function getNetworkValueList(){
		$values = array(
			array("lwn", "rwn", "nwn"),
			array("lcn", "rcn", "ncn")
		);
		return $values;
	}

	public static function getRouteValueList(){
		$values = array("hiking", "foot", "wheelchair", "horse", "ski", "mtb", "bicycle", "piste");
		return $values;
	}

	public static function getCompleteValueList(){
		$values = array("yes", "no");
		return $values;
	}

	public static function getOsmcValueList(){
		$values = array(
			"b_color" => array("orange",  "yellow", "white"),
			"f_color" => array("yellow", "green", "red", "white", "blue", "black", "orange", "purple"),
			"symbol"  => array("corner", "bar", "backslash", "dot", "wheelchair", "triangle", "bowl", "L", "T")
		);
		return $values;
	}

	/**
	 * tabulka kompatibility mezi hodnotami tagu network a kct_barva
	 * @return array
	 */
	public static function getNetworkKctTable(){
	    return array(
	        "major" =>              array("lwn" => 1, "rwn" => 1, "nwn" => 1),
	        "local" =>              array("lwn" => 1, "rwn" => 0, "nwn" => 0),
	        "learning" =>           array("lwn" => 1, "rwn" => 0, "nwn" => 0),
	        "ski" =>                array("lwn" => 1, "rwn" => 1, "nwn" => 0),
	        "wheelchair" =>         array("lwn" => 1, "rwn" => 0, "nwn" => 0),
	        "horse" =>              array("lwn" => 1, "rwn" => 1, "nwn" => 0),
	        "peak" =>               array("lwn" => 1, "rwn" => 0, "nwn" => 0),
	        "ruin" =>               array("lwn" => 1, "rwn" => 0, "nwn" => 0),
	        "spring" =>             array("lwn" => 1, "rwn" => 0, "nwn" => 0),
	        "interesting_object" => array("lwn" => 1, "rwn" => 0, "nwn" => 0),
	    );
	}

	/**
	 * tabulka kompatibility mezi hodnotami tagu route a kct_barva
	 * @return array
	 */
	public static function getRouteKctTable(){
	    return array(
	        "foot" =>       array("major", "local", "learning", "peak", "ruin", "spring", "interesting_object"),
	        "hiking" =>     array("major", "local", "learning", "peak", "ruin", "spring", "interesting_object"),
	        "ski" =>        array("ski"),
	        "wheelchair" => array("wheelchair"),
	        "bicycle"=>     array("bicycle"),
	        "horse"=>       array("horse"),
	        "mtb"=>         array("bicycle"),
	        "piste"=>       array("ski")
	    );
	}

	/**
	 * tabulka kompatibility mezi hodnotami tagu route a osmc:symbol
	 * @return array
	 */
	public static function getRouteOsmcTable(){
	    return array(
	        "foot" =>       array("white", "bar", "corner", "backslash", "triangle", "L", "bowl", "T"),
	        "hiking" =>     array("white", "bar", "corner", "backslash", "triangle", "L", "bowl", "T"),
	        "ski" =>        array("orange", "bar"),
	        "wheelchair" => array("white", "wheelchair"),
	        "bicycle"=>     array("yellow", "bar"),
	        "horse"=>       array("white", "dot"),
	        "mtb"=>         array("yellow", "bar"),
	        "piste"=>       array("orange", "bar")
	    );
	}

	/**
	 * tabulka kompatibility mezi hodnotami tagu osmc:symbol a kct_barva
	 * @return array
	 */
	public static function getKctOsmcTable(){
	    return array(
	        "major" =>              array("bar","white"),
	        "local" =>              array("corner","white"),
	        "learning" =>           array("backslash","white"),
	        "ski" =>                array("bar", "orange"),
	        "wheelchair" =>         array("wheelchair","white"),
	        "horse" =>              array("dot","white"),
	        "peak" =>               array("triangle","white"),
	        "ruin" =>               array("L","white"),
	        "spring" =>             array("bowl","white"),
	        "interesting_object" => array("T","white"),
	        "bicycle" =>            array("bar", "yellow")
	    );
	}
}