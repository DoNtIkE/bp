<?php
class Stats {
	
	protected $db;

	public function __construct(Connector $db){
		$this->db = $db;
	}

	public function getGraphsNames(){
		$sql = "SELECT DISTINCT name FROM tests";
		$result = $this->db->query($sql);
		$ret = array();
		if($result){
			while($row = pg_fetch_assoc($result)){
				$ret[] = $row["name"];
			}
		}
		return $ret;
	}

	public function getGraphsByName($name){
		$sql = "SELECT * FROM tests WHERE name = '".$name."'";
		$result = $this->db->query($sql);
		return $this->db->fetchAllAssoc($result);
	}

	public function getGraph(int $test_id){
		$sql = "SELECT * FROM tests WHERE id = '".$test_id."'";
		return $this->db->selectOneRow($sql);
	}

	public function getMaxCountOfGraph($name){
		$sql = "SELECT MAX(count) FROM test_stats WHERE test_id IN(SELECT id FROM tests WHERE name='".$name."')";
		$result = $this->db->selectOne($sql);
		if($result) return $result;
		return 0;
	}

	protected function getData(int $test_id, $interval){
		$sql = "SELECT 	date, success FROM test_stats WHERE test_id = '".$test_id."'";
		$result = $this->db->query($sql);
		$ret = array();
		if($result){
			while($row = pg_fetch_assoc($result)){
				$ret[strtotime($row['date'])] = $row['success'];
			}
		}
		return $ret;
	}

	protected function generateLabels($interval){
		$fr = strtotime(date("Y-m-d"))-(24*60*60*$interval);
		$t = time();
		for(;$fr <= $t; $fr += 60*60*24){

			$labels[] = date('Y-m-d', $fr);
			
			$lab[] = $fr;
		}

		return $labels;
	}

	public function getDataGraph($name, $interval){
		$colors = array('#e2431e', '#1c91c0', '#f1ca3a' ,'#6f9654','#43459d' ,'#e7711b' );
		$graphs = $this->getGraphsByName($name);

		$i = 0;
		$labels = $this->generateLabels($interval);
		foreach($graphs as $graph){
			$data = array();
			$tmp = $this->getData($graph['id'], $interval);

			foreach ($labels as $value) {
				if(isset($tmp[strtotime($value)])){
					$data[] = $tmp[strtotime($value)];
				}
				else{
					$data[] = "undefined";
				}
			}

			$chart['datasets'][] = array("data" => $data, 
			"label" => $graph["type"], 
			"fill" => false,
	        "lineTension" => 0,
	        "borderColor" => $colors[$i],
	        "borderCapStyle" => 'butt',
	        "borderDash" => [],
	        "borderDashOffset" => 0.0,
	        "borderJoinStyle" => 'miter',
	        "borderWidth" => 2,
	        "pointBorderWidth" => 1,
	        "pointHoverRadius" => 1,
	        "pointHoverBackgroundColor" => "rgba(75,192,192,1)",
	        "pointHoverBorderColor" => "rgba(220,220,220,1)",
	        "pointHoverBorderWidth" => 2,
	        "pointRadius" => 0,
	        "pointHitRadius" => 5,
			);

			$i++;
		}

		$chart['labels'] = $labels;
		return $chart;
	}
}