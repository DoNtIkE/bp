<?php
class Configuration{
	private $data;
	private $CONF_DIR = __DIR__."/../config/";

	public function __construct(){
		// Vývojová verze nebo produkční?
		if($_SERVER["REMOTE_ADDR"] == "127.0.0.1"){
			define("LOCAL", true);
		}
		else{
			define("LOCAL", false);
		}

		define('BASE', dirname(str_replace(array('/ajax', '/xml', '/import', '/ssl', '/cron'), '', $_SERVER['SCRIPT_FILENAME'])).'/');
		define('PAGE', "http://".$_SERVER['HTTP_HOST']."/");

		define('DATA_BASE', BASE.'data/');
		define('DATA_PAGE', PAGE.'data/');

		define('EXPORT_BASE', DATA_BASE.'export/');
		define('EXPORT_PAGE', DATA_PAGE.'export/');

		$this->loadConfiguration();
	}

	private function loadConfiguration(){
		if(LOCAL){
			$conf_file = "config.local.json";
		}
		else{
			$conf_file = "config.json";
		}

		if(file_exists($this->CONF_DIR.$conf_file)){
			$this->data = json_decode(file_get_contents($this->CONF_DIR.$conf_file));
			if($this->data == NULL){	
				throw new ConfigurationException("Configure file parse error.");
			}
		}
		else{
			throw new ConfigurarionException("Configure file not exist");
		}
	}

	public function get($name){
		if($this->data != NULL){
			return $this->data->$name;
		}
	}
}