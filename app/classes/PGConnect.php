<?php
class PGConnect extends Connector{

	private static $instance = null;
	private $server, $user, $password, $database, $schema;

	/**
	 * Execute SQL query
	 * @param String $sql SQL Query
	 * @return boolean/ResouceID 
	 */
	public function query($sql){
		$result = pg_query($this->connection, $sql);
		if(pg_num_rows($result)){
			if(substr(strtolower($sql), 0, 6) == "insert") return $result;

			return $result;
		}
		return false;
	}

	/**
	 * Select one row from result
	 * @param String $sql SQL query
	 * @return mixed boolean/Array return array or false if query faild
	 */
	public function selectOne($sql){
		$result = $this->query($sql);
		if(pg_num_rows($result)){
			return pg_fetch_array($result)[0];
		}
		return false;
	}

	/**
	 * Select one value from first row of result
	 * @param String $sql SQL query
	 * @return mixed boolean/String return String or false if query faild
	 */
	public function selectOneRow($sql){
		$result = $this->query($sql);
		if(pg_num_rows($result)){
			return pg_fetch_assoc($result);
		}
		return false;
	}

	/**
	 * Fetch all rows from result
	 * @param $result Result of PG query
	 * @return array fetched assoc array
	 */
	public function fetchAllAssoc($result){
		$ret = array();
		if($result){
			while($row = pg_fetch_assoc($result)){
				$ret[] = $row;
			}
		}
		return $ret;
	}

	/**
	 * Static function to get instance of this class
	 * @param String $server
	 * @param String $user
	 * @param String $password
	 * @param String $database
	 * @param null/String $schema
	 * @return instance of this class
	 */
	public static function getInstance($server, $user, $password, $database, $schema = null) {
		if(!self::$instance) {
			self::$instance = new self($server, $user, $password, $database, $schema);
		}
		return self::$instance;
	}

	/**
	 * Create instance of this class
	 * @param String $server
	 * @param String $user
	 * @param String $password
	 * @param String $database
	 * @param null/String $schema
	 * @throws ConnectException 
	 * @return instance of this class
	 */
	public function __construct($server, $user, $password, $database, $schema = null){
		$this->connection = @pg_connect("host=".$server." dbname=".$database." user=".$user." password=".$password);

		if(!$this->connection){
			throw new ConnectException;
		}
		if($schema != null){
			$this->query("SET search_path TO ".$schema.";");
		}

		$this->server = $server;
		$this->user = $user;
		$this->password = $password;
		$this->database = $database;
		$this->schema = $schema;
	}
}