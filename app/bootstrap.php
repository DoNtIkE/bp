<?php
session_start();

spl_autoload_register(function ($class) {
	if(file_exists(__DIR__.'/classes/'.$class.'.php')){
    	include __DIR__.'/classes/'.$class.'.php';
	}
	else if(preg_match('/Test$/', $class)){
    	include __DIR__.'/classes/tests/'.$class.'.php';
	}
	else if(preg_match('/Controller$/', $class)){
    	include __DIR__.'/controller/'.$class.'.php';
	}
	else if(preg_match('/Exception$/', $class)){
    	include __DIR__.'/classes/exceptions/'.$class.'.php';
	}
});

// Odchytávání všech vyjímek v aplikaci
function ExceptionHandler($e){
	if(LOCAL){
		echo "<pre>";
		echo $e;
		echo "</pre>";
	}
	else{
		if(file_exists(BASE.'500.html')){
			http_response_code(500);
			echo file_get_contents(BASE.'500.html');
		}
	}
}
set_exception_handler('ExceptionHandler');

// Odchycení error a warning a převod na vyjímku
function ErrorToException($code, $message, $file, $line, $context){
    throw new ErrorOrWarningException($code, $message, $file, $line, $context);
}
set_error_handler('ErrorToException');

// Load configure
$conf = new Configuration();
date_default_timezone_set($conf->get("TIME_ZONE"));

// Connect to DB
$db = PGConnect::getInstance($conf->get("DB_SERVER"), $conf->get("DB_USER"), $conf->get("DB_PASSWORD"), $conf->get("DB_DATABASE"), $conf->get("DB_SCHEME"));

$router = new RouterController($db, $conf);
$url = $router->parseUrl($_SERVER["REQUEST_URI"]);

$router->control($url);
$router->printView();